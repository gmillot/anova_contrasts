Strain	Condition	Gene	Replicat.biologique	Ct	Ct.mean	Delta.Ct	Delta.Ct.mean2	Delta.Delta.Ct	Delta.Ct.mean
WT	THY	gsp	r1	27.35	27.26	2.8	2.78	0	2.7800000
WT	THY	gsp	r1	27.25		2.81			
WT	THY	gsp	r1	27.17		2.73			
WT	THY.AnTc	gsp	r1	30.45	30.35	5.54	5.51	2.73	5.5133333
WT	THY.AnTc	gsp	r1	30.29		5.48			
WT	THY.AnTc	gsp	r1	30.3		5.52			
Delta.grp	THY	gsp	r1	24.16	24.1	-0.25	-0.26	-3.04	-0.2600000
Delta.grp	THY	gsp	r1	24.02		-0.33			
Delta.grp	THY	gsp	r1	24.12		-0.2			
Delta.grp	THY.AnTc	gsp	r1	27.29	27.43	2.96	3.07	0.29	3.0666667
Delta.grp	THY.AnTc	gsp	r1	27.46		3.09			
Delta.grp	THY.AnTc	gsp	r1	27.53		3.15			
WT	THY	gsp	r2	27.31	27.72	2.52	3.23	0	3.2300000
WT	THY	gsp	r2	27.26		2.92			
WT	THY	gsp	r2	28.58		4.25			
WT	THY.AnTc	gsp	r2	29.62	29.63	5.17	5.18	1.95	5.1766667
WT	THY.AnTc	gsp	r2	29.79		5.3			
WT	THY.AnTc	gsp	r2	29.48		5.06			
Delta.grp	THY	gsp	r2	23.69	23.68	-0.51	-0.61	-3.84	-0.6100000
Delta.grp	THY	gsp	r2	23.57		-0.83			
Delta.grp	THY	gsp	r2	23.79		-0.49			
Delta.grp	THY.AnTc	gsp	r2	27.48	27.69	2.69	2.89	-0.34	2.8866667
Delta.grp	THY.AnTc	gsp	r2	27.63		2.89			
Delta.grp	THY.AnTc	gsp	r2	27.97		3.08			
WT	THY	gsp	r3	27.33	27.29	2.32	2.31	0	2.3133333
WT	THY	gsp	r3	27.39		2.39			
WT	THY	gsp	r3	27.16		2.23			
WT	THY.AnTc	gsp	r3	29.44	29.43	4.65	4.71	2.4	4.7166667
WT	THY.AnTc	gsp	r3	29.42		4.69			
WT	THY.AnTc	gsp	r3	29.43		4.81			
Delta.grp	THY	gsp	r3	23.36	23.44	-1.22	-1.2	-3.52	-1.2033333
Delta.grp	THY	gsp	r3	23.51		-1.13			
Delta.grp	THY	gsp	r3	23.47		-1.26			
Delta.grp	THY.AnTc	gsp	r3	26.73	26.77	2.16	2.17	-0.14	2.1766667
Delta.grp	THY.AnTc	gsp	r3	26.68		2.17			
Delta.grp	THY.AnTc	gsp	r3	26.88		2.2			
WT	THY	rpoB	r1	24.55	24.48	0	0	0	0.0000000
WT	THY	rpoB	r1	24.45		0			
WT	THY	rpoB	r1	24.44		0			
WT	THY.AnTc	rpoB	r1	24.91	24.84	0	0	0	0.0000000
WT	THY.AnTc	rpoB	r1	24.81		0			
WT	THY.AnTc	rpoB	r1	24.78		0			
Delta.grp	THY	rpoB	r1	24.41	24.36	0	0	0	0.0000000
Delta.grp	THY	rpoB	r1	24.35		0			
Delta.grp	THY	rpoB	r1	24.32		0			
Delta.grp	THY.AnTc	rpoB	r1	24.33	24.36	0	0	0	0.0000000
Delta.grp	THY.AnTc	rpoB	r1	24.37		0			
Delta.grp	THY.AnTc	rpoB	r1	24.38		0			
WT	THY	rpoB	r2	24.79	24.49	0	0	0	0.0000000
WT	THY	rpoB	r2	24.34		0			
WT	THY	rpoB	r2	24.33		0			
WT	THY.AnTc	rpoB	r2	24.45	24.45	0	0	0	0.0000000
WT	THY.AnTc	rpoB	r2	24.49		0			
WT	THY.AnTc	rpoB	r2	24.42		0			
Delta.grp	THY	rpoB	r2	24.2	24.29	0	0	0	0.0000000
Delta.grp	THY	rpoB	r2	24.4		0			
Delta.grp	THY	rpoB	r2	24.27		0			
Delta.grp	THY.AnTc	rpoB	r2	24.79	24.81	0	0	0	0.0000000
Delta.grp	THY.AnTc	rpoB	r2	24.74		0			
Delta.grp	THY.AnTc	rpoB	r2	24.88		0			
WT	THY	rpoB	r3	25	24.98	0	0	0	0.0000000
WT	THY	rpoB	r3	25		0			
WT	THY	rpoB	r3	24.93		0			
WT	THY.AnTc	rpoB	r3	24.79	24.72	0	0	0	0.0000000
WT	THY.AnTc	rpoB	r3	24.73		0			
WT	THY.AnTc	rpoB	r3	24.62		0			
Delta.grp	THY	rpoB	r3	24.58	24.65	0	0	0	0.0000000
Delta.grp	THY	rpoB	r3	24.64		0			
Delta.grp	THY	rpoB	r3	24.73		0			
Delta.grp	THY.AnTc	rpoB	r3	24.58	24.59	0	0	0	0.0000000
Delta.grp	THY.AnTc	rpoB	r3	24.51		0			
Delta.grp	THY.AnTc	rpoB	r3	24.69		0			
WT	THY	gallo_rs03700	r1	25.69	25.58	1.13	1.1	0	1.1000000
WT	THY	gallo_rs03700	r1	25.6		1.15			
WT	THY	gallo_rs03700	r1	25.46		1.02			
WT	THY.AnTc	gallo_rs03700	r1	28.35	28.41	3.43	3.57	2.47	3.5700000
WT	THY.AnTc	gallo_rs03700	r1	28.4		3.58			
WT	THY.AnTc	gallo_rs03700	r1	28.48		3.7			
Delta.grp	THY	gallo_rs03700	r1	22.45	22.4	-1.97	-1.96	-3.06	-1.9600000
Delta.grp	THY	gallo_rs03700	r1	22.33		-2.01			
Delta.grp	THY	gallo_rs03700	r1	22.43		-1.9			
Delta.grp	THY.AnTc	gallo_rs03700	r1	25.7	25.86	1.37	1.5	0.4	1.5066667
Delta.grp	THY.AnTc	gallo_rs03700	r1	25.95		1.58			
Delta.grp	THY.AnTc	gallo_rs03700	r1	25.95		1.57			
WT	THY	gallo_rs03700	r2	25.24	25.23	0.45	0.74	0	0.7400000
WT	THY	gallo_rs03700	r2	25.21		0.87			
WT	THY	gallo_rs03700	r2	25.24		0.9			
WT	THY.AnTc	gallo_rs03700	r2	27.7	27.74	3.25	3.29	2.55	3.2833333
WT	THY.AnTc	gallo_rs03700	r2	27.8		3.31			
WT	THY.AnTc	gallo_rs03700	r2	27.72		3.29			
Delta.grp	THY	gallo_rs03700	r2	22.48	22.44	-1.73	-1.85	-2.59	-1.8566667
Delta.grp	THY	gallo_rs03700	r2	22.41		-1.99			
Delta.grp	THY	gallo_rs03700	r2	22.42		-1.85			
Delta.grp	THY.AnTc	gallo_rs03700	r2	25.95	26.05	1.16	1.24	0.5	1.2400000
Delta.grp	THY.AnTc	gallo_rs03700	r2	26		1.26			
Delta.grp	THY.AnTc	gallo_rs03700	r2	26.19		1.3			
WT	THY	gallo_rs03700	r3	26.81	26.14	1.81	1.17	0	1.1666667
WT	THY	gallo_rs03700	r3	25.93		0.93			
WT	THY	gallo_rs03700	r3	25.69		0.76			
WT	THY.AnTc	gallo_rs03700	r3	28.09	28.1	3.3	3.38	2.21	3.3800000
WT	THY.AnTc	gallo_rs03700	r3	28		3.27			
WT	THY.AnTc	gallo_rs03700	r3	28.19		3.57			
Delta.grp	THY	gallo_rs03700	r3	22.29	22.37	-2.29	-2.28	-3.45	-2.2800000
Delta.grp	THY	gallo_rs03700	r3	22.41		-2.22			
Delta.grp	THY	gallo_rs03700	r3	22.4		-2.33			
Delta.grp	THY.AnTc	gallo_rs03700	r3	25.36	25.44	0.79	0.85	-0.31	0.8533333
Delta.grp	THY.AnTc	gallo_rs03700	r3	25.36		0.85			
Delta.grp	THY.AnTc	gallo_rs03700	r3	25.61		0.92			
WT	THY	gallo_rs10335	r1	25.34	25.36	0.79	0.88	0	0.8800000
WT	THY	gallo_rs10335	r1	25.4		0.95			
WT	THY	gallo_rs10335	r1	25.35		0.9			
WT	THY.AnTc	gallo_rs10335	r1	28.15	28.1	3.24	3.27	2.39	3.2700000
WT	THY.AnTc	gallo_rs10335	r1	28.09		3.27			
WT	THY.AnTc	gallo_rs10335	r1	28.08		3.3			
Delta.grp	THY	gallo_rs10335	r1	23.23	23.58	-1.18	-0.79	-1.67	-0.7866667
Delta.grp	THY	gallo_rs10335	r1	23.21		-1.14			
Delta.grp	THY	gallo_rs10335	r1	24.28		-0.04			
Delta.grp	THY.AnTc	gallo_rs10335	r1	25.88	25.92	1.55	1.56	0.68	1.5633333
Delta.grp	THY.AnTc	gallo_rs10335	r1	25.93		1.56			
Delta.grp	THY.AnTc	gallo_rs10335	r1	25.96		1.58			
WT	THY	gallo_rs10335	r2	25.36	25.13	0.56	0.64	0	0.6333333
WT	THY	gallo_rs10335	r2	24.97		0.62			
WT	THY	gallo_rs10335	r2	25.06		0.72			
WT	THY.AnTc	gallo_rs10335	r2	27.55	27.54	3.1	3.09	2.45	3.0933333
WT	THY.AnTc	gallo_rs10335	r2	27.53		3.05			
WT	THY.AnTc	gallo_rs10335	r2	27.55		3.13			
Delta.grp	THY	gallo_rs10335	r2	22.64	22.71	-1.56	-1.58	-2.22	-1.5800000
Delta.grp	THY	gallo_rs10335	r2	22.73		-1.67			
Delta.grp	THY	gallo_rs10335	r2	22.76		-1.51			
Delta.grp	THY.AnTc	gallo_rs10335	r2	25.81	25.83	1.02	1.03	0.39	1.0300000
Delta.grp	THY.AnTc	gallo_rs10335	r2	25.81		1.07			
Delta.grp	THY.AnTc	gallo_rs10335	r2	25.88		1			
WT	THY	gallo_rs10335	r3	25.26	25.32	0.26	0.34	0	0.3400000
WT	THY	gallo_rs10335	r3	25.35		0.35			
WT	THY	gallo_rs10335	r3	25.34		0.41			
WT	THY.AnTc	gallo_rs10335	r3	27.15	27.16	2.36	2.44	2.1	2.4466667
WT	THY.AnTc	gallo_rs10335	r3	27.25		2.52			
WT	THY.AnTc	gallo_rs10335	r3	27.08		2.46			
Delta.grp	THY	gallo_rs10335	r3	22.61	22.62	-1.97	-2.03	-2.37	-2.0333333
Delta.grp	THY	gallo_rs10335	r3	22.61		-2.03			
Delta.grp	THY	gallo_rs10335	r3	22.63		-2.1			
Delta.grp	THY.AnTc	gallo_rs10335	r3	25.06	25.12	0.49	0.52	0.19	0.5233333
Delta.grp	THY.AnTc	gallo_rs10335	r3	25.06		0.55			
Delta.grp	THY.AnTc	gallo_rs10335	r3	25.22		0.53			
WT	THY	ghk	r1	25.94	25.8	1.39	1.32	0	1.3233333
WT	THY	ghk	r1	25.76		1.31			
WT	THY	ghk	r1	25.71		1.27			
WT	THY.AnTc	ghk	r1	27.84	27.88	2.93	3.05	1.73	3.0466667
WT	THY.AnTc	ghk	r1	27.91		3.09			
WT	THY.AnTc	ghk	r1	27.9		3.12			
Delta.grp	THY	ghk	r1	24.07	24.07	-0.34	-0.29	-1.61	-0.2900000
Delta.grp	THY	ghk	r1	24.03		-0.32			
Delta.grp	THY	ghk	r1	24.11		-0.21			
Delta.grp	THY.AnTc	ghk	r1	26.31	26.34	1.98	1.98	0.66	1.9800000
Delta.grp	THY.AnTc	ghk	r1	26.36		1.99			
Delta.grp	THY.AnTc	ghk	r1	26.36		1.97			
WT	THY	ghk	r2	25.4	25.52	0.6	1.03	0	1.0333333
WT	THY	ghk	r2	25.54		1.2			
WT	THY	ghk	r2	25.63		1.3			
WT	THY.AnTc	ghk	r2	27.22	27.24	2.77	2.79	1.75	2.7866667
WT	THY.AnTc	ghk	r2	27.31		2.82			
WT	THY.AnTc	ghk	r2	27.19		2.77			
Delta.grp	THY	ghk	r2	23.68	23.61	-0.53	-0.68	-1.72	-0.6833333
Delta.grp	THY	ghk	r2	23.52		-0.87			
Delta.grp	THY	ghk	r2	23.62		-0.65			
Delta.grp	THY.AnTc	ghk	r2	26.36	26.4	1.57	1.6	0.56	1.6000000
Delta.grp	THY.AnTc	ghk	r2	26.39		1.65			
Delta.grp	THY.AnTc	ghk	r2	26.46		1.58			
WT	THY	ghk	r3	26.02	25.93	1.02	0.96	0	0.9566667
WT	THY	ghk	r3	26.07		1.07			
WT	THY	ghk	r3	25.71		0.78			
WT	THY.AnTc	ghk	r3	27.4	27.4	2.6	2.68	1.73	2.6833333
WT	THY.AnTc	ghk	r3	27.37		2.63			
WT	THY.AnTc	ghk	r3	27.44		2.82			
Delta.grp	THY	ghk	r3	23.72	23.79	-0.86	-0.86	-1.81	-0.8566667
Delta.grp	THY	ghk	r3	23.8		-0.83			
Delta.grp	THY	ghk	r3	23.86		-0.88			
Delta.grp	THY.AnTc	ghk	r3	25.95	25.97	1.38	1.38	0.43	1.3866667
Delta.grp	THY.AnTc	ghk	r3	25.88		1.37			
Delta.grp	THY.AnTc	ghk	r3	26.09		1.41			
WT	THY	grr	r1	24.95	24.99	0.4	0.51	0	0.5066667
WT	THY	grr	r1	24.97		0.52			
WT	THY	grr	r1	25.04		0.6			
WT	THY.AnTc	grr	r1	27.14	27.1	2.23	2.26	1.76	2.2633333
WT	THY.AnTc	grr	r1	27.11		2.3			
WT	THY.AnTc	grr	r1	27.04		2.26			
Delta.grp	THY	grr	r1	23.05	23.1	-1.36	-1.26	-1.77	-1.2633333
Delta.grp	THY	grr	r1	23.11		-1.24			
Delta.grp	THY	grr	r1	23.13		-1.19			
Delta.grp	THY.AnTc	grr	r1	25.36	25.43	1.03	1.07	0.56	1.0700000
Delta.grp	THY.AnTc	grr	r1	25.42		1.05			
Delta.grp	THY.AnTc	grr	r1	25.51		1.13			
WT	THY	grr	r2	25.09	24.75	0.29	0.26	0	0.2600000
WT	THY	grr	r2	24.58		0.24			
WT	THY	grr	r2	24.59		0.25			
WT	THY.AnTc	grr	r2	26.44	26.42	1.99	1.96	1.7	1.9633333
WT	THY.AnTc	grr	r2	26.43		1.94			
WT	THY.AnTc	grr	r2	26.38		1.96			
Delta.grp	THY	grr	r2	22.79	22.85	-1.41	-1.44	-1.7	-1.4433333
Delta.grp	THY	grr	r2	22.88		-1.52			
Delta.grp	THY	grr	r2	22.87		-1.4			
Delta.grp	THY.AnTc	grr	r2	25.51	25.55	0.71	0.75	0.48	0.7433333
Delta.grp	THY.AnTc	grr	r2	25.57		0.82			
Delta.grp	THY.AnTc	grr	r2	25.58		0.7			
WT	THY	grr	r3	25.22	25.14	0.22	0.16	0	0.1633333
WT	THY	grr	r3	25.16		0.16			
WT	THY	grr	r3	25.04		0.11			
WT	THY.AnTc	grr	r3	26.6	26.54	1.81	1.82	1.66	1.8233333
WT	THY.AnTc	grr	r3	26.46		1.72			
WT	THY.AnTc	grr	r3	26.56		1.94			
Delta.grp	THY	grr	r3	23.09	23.08	-1.49	-1.57	-1.74	-1.5700000
Delta.grp	THY	grr	r3	23.06		-1.57			
Delta.grp	THY	grr	r3	23.08		-1.65			
Delta.grp	THY.AnTc	grr	r3	25.12	25.16	0.54	0.57	0.41	0.5666667
Delta.grp	THY.AnTc	grr	r3	25.12		0.61			
Delta.grp	THY.AnTc	grr	r3	25.24		0.55			
WT	THY	gallo_rs10370	r1	26.59	26.51	2.04	2.03	0	2.0266667
WT	THY	gallo_rs10370	r1	26.46		2.02			
WT	THY	gallo_rs10370	r1	26.47		2.02			
WT	THY.AnTc	gallo_rs10370	r1	29.72	29.65	4.8	4.82	2.79	4.8200000
WT	THY.AnTc	gallo_rs10370	r1	29.69		4.88			
WT	THY.AnTc	gallo_rs10370	r1	29.56		4.78			
Delta.grp	THY	gallo_rs10370	r1	24.11	24.08	-0.3	-0.28	-2.31	-0.2800000
Delta.grp	THY	gallo_rs10370	r1	24.02		-0.33			
Delta.grp	THY	gallo_rs10370	r1	24.12		-0.21			
Delta.grp	THY.AnTc	gallo_rs10370	r1	27.05	27.07	2.72	2.71	0.68	2.7066667
Delta.grp	THY.AnTc	gallo_rs10370	r1	27		2.63			
Delta.grp	THY.AnTc	gallo_rs10370	r1	27.15		2.77			
WT	THY	gallo_rs10370	r2	25.96	26.04	1.17	1.55	0	1.5533333
WT	THY	gallo_rs10370	r2	26.02		1.68			
WT	THY	gallo_rs10370	r2	26.15		1.81			
WT	THY.AnTc	gallo_rs10370	r2	28.71	28.74	4.26	4.28	2.73	4.2833333
WT	THY.AnTc	gallo_rs10370	r2	28.82		4.33			
WT	THY.AnTc	gallo_rs10370	r2	28.68		4.26			
Delta.grp	THY	gallo_rs10370	r2	23.54	23.49	-0.66	-0.8	-2.35	-0.7966667
Delta.grp	THY	gallo_rs10370	r2	23.4		-1			
Delta.grp	THY	gallo_rs10370	r2	23.55		-0.73			
Delta.grp	THY.AnTc	gallo_rs10370	r2	27.01	27.04	2.22	2.23	0.68	2.2366667
Delta.grp	THY.AnTc	gallo_rs10370	r2	27.04		2.29			
Delta.grp	THY.AnTc	gallo_rs10370	r2	27.08		2.2			
WT	THY	gallo_rs10370	r3	26.31	26.22	1.31	1.24	0	1.2400000
WT	THY	gallo_rs10370	r3	26.28		1.28			
WT	THY	gallo_rs10370	r3	26.06		1.13			
WT	THY.AnTc	gallo_rs10370	r3	28.52	28.55	3.72	3.84	2.6	3.8366667
WT	THY.AnTc	gallo_rs10370	r3	28.54		3.81			
WT	THY.AnTc	gallo_rs10370	r3	28.6		3.98			
Delta.grp	THY	gallo_rs10370	r3	23.28	23.37	-1.3	-1.28	-2.51	-1.2733333
Delta.grp	THY	gallo_rs10370	r3	23.43		-1.2			
Delta.grp	THY	gallo_rs10370	r3	23.41		-1.32			
Delta.grp	THY.AnTc	gallo_rs10370	r3	26.13	26.15	1.55	1.56	0.33	1.5633333
Delta.grp	THY.AnTc	gallo_rs10370	r3	26.07		1.56			
Delta.grp	THY.AnTc	gallo_rs10370	r3	26.27		1.58			
WT	THY	glpA	r1	22.33	22.32	-2.22	-2.16	0	-2.1566667
WT	THY	glpA	r1	22.33		-2.12			
WT	THY	glpA	r1	22.31		-2.13			
WT	THY.AnTc	glpA	r1	24.49	24.48	-0.42	-0.36	1.8	-0.3566667
WT	THY.AnTc	glpA	r1	24.51		-0.31			
WT	THY.AnTc	glpA	r1	24.44		-0.34			
Delta.grp	THY	glpA	r1	20.88	20.87	-3.54	-3.49	-1.33	-3.4933333
Delta.grp	THY	glpA	r1	20.85		-3.5			
Delta.grp	THY	glpA	r1	20.88		-3.44			
Delta.grp	THY.AnTc	glpA	r1	22.28	22.33	-2.05	-2.03	0.13	-2.0333333
Delta.grp	THY.AnTc	glpA	r1	22.29		-2.08			
Delta.grp	THY.AnTc	glpA	r1	22.41		-1.97			
WT	THY	glpA	r2	22.4	22.03	-2.4	-2.45	0	-2.4566667
WT	THY	glpA	r2	21.89		-2.45			
WT	THY	glpA	r2	21.81		-2.52			
WT	THY.AnTc	glpA	r2	23.92	23.96	-0.53	-0.49	1.96	-0.4933333
WT	THY.AnTc	glpA	r2	24		-0.49			
WT	THY.AnTc	glpA	r2	23.97		-0.46			
Delta.grp	THY	glpA	r2	20.58	20.65	-3.62	-3.64	-1.18	-3.6366667
Delta.grp	THY	glpA	r2	20.66		-3.74			
Delta.grp	THY	glpA	r2	20.72		-3.55			
Delta.grp	THY.AnTc	glpA	r2	22.34	22.42	-2.45	-2.39	0.07	-2.3900000
Delta.grp	THY.AnTc	glpA	r2	22.44		-2.31			
Delta.grp	THY.AnTc	glpA	r2	22.47		-2.41			
WT	THY	glpA	r3	22.17	22.09	-2.84	-2.89	0	-2.8933333
WT	THY	glpA	r3	22.1		-2.9			
WT	THY	glpA	r3	21.99		-2.94			
WT	THY.AnTc	glpA	r3	23.89	23.75	-0.91	-0.97	1.92	-0.9666667
WT	THY.AnTc	glpA	r3	23.82		-0.91			
WT	THY.AnTc	glpA	r3	23.55		-1.08			
Delta.grp	THY	glpA	r3	20.29	20.34	-4.29	-4.31	-1.41	-4.3066667
Delta.grp	THY	glpA	r3	20.39		-4.25			
Delta.grp	THY	glpA	r3	20.35		-4.38			
Delta.grp	THY.AnTc	glpA	r3	21.87	21.98	-2.7	-2.61	0.29	-2.6066667
Delta.grp	THY.AnTc	glpA	r3	21.99		-2.52			
Delta.grp	THY.AnTc	glpA	r3	22.09		-2.6			
WT	THY	gltA	r1	26.19	26.14	1.64	1.66	0	1.6633333
WT	THY	gltA	r1	26.13		1.68			
WT	THY	gltA	r1	26.11		1.67			
WT	THY.AnTc	gltA	r1	28.46	28.44	3.55	3.6	1.94	3.6066667
WT	THY.AnTc	gltA	r1	28.4		3.59			
WT	THY.AnTc	gltA	r1	28.46		3.68			
Delta.grp	THY	gltA	r1	24.08	24.05	-0.33	-0.31	-1.98	-0.3133333
Delta.grp	THY	gltA	r1	24.03		-0.32			
Delta.grp	THY	gltA	r1	24.04		-0.29			
Delta.grp	THY.AnTc	gltA	r1	26.3	26.37	1.97	2.01	0.34	2.0066667
Delta.grp	THY.AnTc	gltA	r1	26.38		2.01			
Delta.grp	THY.AnTc	gltA	r1	26.42		2.04			
WT	THY	gltA	r2	25.8	25.89	1	1.4	0	1.4000000
WT	THY	gltA	r2	25.87		1.53			
WT	THY	gltA	r2	26.01		1.67			
WT	THY.AnTc	gltA	r2	27.83	27.78	3.38	3.32	1.92	3.3233333
WT	THY.AnTc	gltA	r2	27.89		3.4			
WT	THY.AnTc	gltA	r2	27.61		3.19			
Delta.grp	THY	gltA	r2	23.59	23.55	-0.61	-0.74	-2.14	-0.7400000
Delta.grp	THY	gltA	r2	23.5		-0.9			
Delta.grp	THY	gltA	r2	23.56		-0.71			
Delta.grp	THY.AnTc	gltA	r2	26.39	26.4	1.59	1.59	0.19	1.5900000
Delta.grp	THY.AnTc	gltA	r2	26.34		1.59			
Delta.grp	THY.AnTc	gltA	r2	26.48		1.59			
WT	THY	gltA	r3	26.28	26.27	1.28	1.29	0	1.2900000
WT	THY	gltA	r3	26.42		1.42			
WT	THY	gltA	r3	26.1		1.17			
WT	THY.AnTc	gltA	r3	27.83	27.76	3.04	3.05	1.76	3.0466667
WT	THY.AnTc	gltA	r3	27.74		3.01			
WT	THY.AnTc	gltA	r3	27.71		3.09			
Delta.grp	THY	gltA	r3	23.56	23.66	-1.02	-0.99	-2.28	-0.9833333
Delta.grp	THY	gltA	r3	23.69		-0.95			
Delta.grp	THY	gltA	r3	23.75		-0.98			
Delta.grp	THY.AnTc	gltA	r3	25.79	25.81	1.22	1.22	-0.08	1.2166667
Delta.grp	THY.AnTc	gltA	r3	25.71		1.2			
Delta.grp	THY.AnTc	gltA	r3	25.91		1.23			
WT	THY	gallo_rs10400	r1	30.1	30.06	5.55	5.58	0	5.5733333
WT	THY	gallo_rs10400	r1	30.13		5.68			
WT	THY	gallo_rs10400	r1	29.94		5.49			
WT	THY.AnTc	gallo_rs10400	r1	32.16	32.15	7.25	7.32	1.74	7.3166667
WT	THY.AnTc	gallo_rs10400	r1	32.26		7.45			
WT	THY.AnTc	gallo_rs10400	r1	32.03		7.25			
Delta.grp	THY	gallo_rs10400	r1	28.16	28.19	3.75	3.83	-1.74	3.8333333
Delta.grp	THY	gallo_rs10400	r1	28.17		3.82			
Delta.grp	THY	gallo_rs10400	r1	28.25		3.93			
Delta.grp	THY.AnTc	gallo_rs10400	r1	30.23	30.36	5.9	5.99	0.42	5.9966667
Delta.grp	THY.AnTc	gallo_rs10400	r1	30.27		5.9			
Delta.grp	THY.AnTc	gallo_rs10400	r1	30.57		6.19			
WT	THY	gallo_rs10400	r2	30.28	30.03	5.49	5.54	0	5.5400000
WT	THY	gallo_rs10400	r2	29.81		5.47			
WT	THY	gallo_rs10400	r2	29.99		5.66			
WT	THY.AnTc	gallo_rs10400	r2	31.71	31.68	7.26	7.22	1.69	7.2233333
WT	THY.AnTc	gallo_rs10400	r2	31.76		7.27			
WT	THY.AnTc	gallo_rs10400	r2	31.57		7.14			
Delta.grp	THY	gallo_rs10400	r2	28	28.03	3.8	3.74	-1.79	3.7433333
Delta.grp	THY	gallo_rs10400	r2	28.08		3.68			
Delta.grp	THY	gallo_rs10400	r2	28.02		3.75			
Delta.grp	THY.AnTc	gallo_rs10400	r2	30.38	30.51	5.59	5.71	0.17	5.7066667
Delta.grp	THY.AnTc	gallo_rs10400	r2	30.5		5.75			
Delta.grp	THY.AnTc	gallo_rs10400	r2	30.66		5.78			
WT	THY	gallo_rs10400	r3	30.3	30.28	5.3	5.3	0	5.3033333
WT	THY	gallo_rs10400	r3	30.27		5.27			
WT	THY	gallo_rs10400	r3	30.27		5.34			
WT	THY.AnTc	gallo_rs10400	r3	31.68	31.77	6.88	7.05	1.75	7.0500000
WT	THY.AnTc	gallo_rs10400	r3	32.03		7.3			
WT	THY.AnTc	gallo_rs10400	r3	31.59		6.97			
Delta.grp	THY	gallo_rs10400	r3	28.18	28.13	3.6	3.48	-1.82	3.4833333
Delta.grp	THY	gallo_rs10400	r3	28.08		3.44			
Delta.grp	THY	gallo_rs10400	r3	28.14		3.41			
Delta.grp	THY.AnTc	gallo_rs10400	r3	30	30.07	5.42	5.48	0.17	5.4766667
Delta.grp	THY.AnTc	gallo_rs10400	r3	30.04		5.53			
Delta.grp	THY.AnTc	gallo_rs10400	r3	30.17		5.48			
									
									
									
									
