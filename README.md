[//]: # "#to make links in gitlab: example with racon https://github.com/isovic/racon"
[//]: # "tricks in markdown: https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown"

| usage | R dependencies |
| --- | --- |
| [![R Version](https://img.shields.io/badge/code-R-blue?style=plastic)](https://cran.r-project.org/mirrors.html) | [![Dependencies: R Version](https://img.shields.io/badge/R-v4.0.2-blue?style=plastic)](https://cran.r-project.org/mirrors.html) |
| [![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses) | [![Dependencies: R Package](https://img.shields.io/badge/package-car%20v3.0.9-blue?style=plastic)](https://cran.r-project.org/web/packages/car/index.html) |
| | [![Dependencies: R Package](https://img.shields.io/badge/package-carData%20v3.0.4-blue?style=plastic)](https://cran.r-project.org/web/packages/carData/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-emmeans%20v1.5.0-blue?style=plastic)](https://cran.r-project.org/web/packages/emmeans/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-ggplot2%20v3.3.2-blue?style=plastic)](https://github.com/tidyverse/ggplot2) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-jpeg%20v0.1_8.1-blue?style=plastic)](https://cran.r-project.org/web/packages/jpeg/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-lemon%20v0.4.5-blue?style=plastic)](https://cran.r-project.org/web/packages/lemon/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-lme4%20v1.1.23-blue?style=plastic)](https://cran.r-project.org/web/packages/lme4/index.html)|
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-lmerTest%20v3.1.2-blue?style=plastic)](https://cran.r-project.org/web/packages/lmerTest/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-lubridate%20v1.7.9-blue?style=plastic)](https://github.com/tidyverse/lubridate) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-Matrix%20v1.2.18-blue?style=plastic)](https://cran.r-project.org/web/packages/Matrix/index.html) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/toolbox-cute%20little%20R%20functions%20v6.0.0-blue?style=plastic)](https://gitlab.pasteur.fr/gmillot/cute_little_R_functions) |

<br /><br />
## TABLE OF CONTENTS

   - [AIM](#aim)
   - [CONTENT](#content)
   - [HOW TO RUN](#how-to-run)
   - [OUTPUT](#output)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [ACKNOWLEDGEMENTS](#Acknowledgements)
   - [WHAT'S NEW IN](#what's-new-in)

<br /><br />
## AIM

Versatile tool for ANOVA and 2 x 2 contrast analysis

Support one-way or two-ways ANOVA, multiple ANOVAs in batch, Type I, II or III, complete random design or block design, balanced or unbalanced design, nested situation, aliased and interacting factors

<br /><br />
## CONTENT

| Files and folder | Description |
| :--- | :--- |
| **anova_contrasts.R** | File that can be executed using a CLI (command line interface) or sourced in R or RStudio. |
| **anova_contrasts.rconfig** | Parameter settings for the anova_contrasts.R file. |
| **dataset folder** | Folder containing some datasets than can be used as examples. |
| **dev folder** | Folder containing data for current or future developments. |
| **example_of_result folder** | Folder containing an example of result obtained with the Internatilzation dataset. |
| **Licence.txt** | Licence of the release. |


<br /><br />
## INPUT

| Required files |
| :--- |
| A folder containing files (.txt, .tsv) containing a sparse counting table. Each file is analyzed independently from the others. |

<br /><br />
## HOW TO RUN anova_contrasts

### Using a R GUI (graphic user interface, i.e., R or RStudio windows)

1) Open the anova_contrasts.rconfig file and set the parameters. The file must be present in the same directory as anova_contrasts.R

2) Open R or RStudio

3) Source the anova_contrasts.R file, for instance using the following instruction:

`  source("C:/Users/Gael/Desktop/anova_contrasts.R")  `


### Using a R CLI (command line interface)

1) Open the anova_contrasts.rconfig file and set the parameters. The file must be present in the same directory as anova_contrasts.R

2) Open a shell windows

3) run anova_contrasts.R, for instance using the following instruction:

`  Rscript anova_contrasts.R anova_contrasts.rconfig  `

For WSL2, use something like:

```
  cd /mnt/c/Users/Gael/Documents/Git_projects/anova_contrasts  
  /mnt/c/Program\ Files/R/R-4.0.2/bin/Rscript anova_contrasts.R anova_contrasts.rconfig  
```


## anova_contrasts OUTPUT

| Mandatory Files | Description |
| --- | --- |
| **anova_contrasts_xxxxxxxxxx_report.txt** | report file |
| **all_objects.RData** | R file containing all the objects created during the run. Open it to inspects the objects |
| **graph.pdf** | some plots, including the distribution of the quantitative variable and analysis of residuals |

| Optional Files | Description |
| --- | --- |
| **modified_dataset.txt** | initial dataset modified by the script (NA removal, log transfo, etc.)|
| **full_contingency_table.txt** | full contingency table (if too big to be printed in the report)|
| **<CLASS_OF_INDEP_QUALI_VAR>_full_contingency_table.txt** | full contingency table for each class of the indep.quali.var parameter (if too big to be printed in the report)|

| Optional Files (non NULL contrast.var parameter) | Description |
| --- | --- |
| **pvalue_results_1.txt** | table of the 2 x 2 contrast tests performed when the model1 parameter is non NULL|
| **pvalue_results_2.txt** | table of the 2 x 2 contrast tests performed when the model2 parameter is non NULL|
| **pvalue_results_3.txt** | table of the 2 x 2 contrast tests performed when the model3 parameter is non NULL|
| **pvalue_results_total.txt** | table of the 2 x 2 contrast tests performed using the different models, if more than one model1, model2, model3 parameters are non NULL|
| **pvalue_results_1.RData** | equivalent to the pvalue_results_1.txt but as R object|
| **pvalue_results_2.RData** | equivalent to the pvalue_results_2.txt but as R object|
| **pvalue_results_3.RData** | equivalent to the pvalue_results_3.txt but as R object|
| **pvalue_results_total.RData** | equivalent to the pvalue_results_total.txt but as R object|

| Optional Files (non NULL model1 parameter and a single predictive variable) | Description |
| --- | --- |
| **pvalue_results_t_test_1.txt** | table of the 2 x 2 t tests with Welch correction |
| **pvalue_results_t_test_1.RData** | equivalent to the pvalue_results_t_test_1.txt but as R object|

| Optional Files (non NULL simple.model parameter) | Description |
| --- | --- |
| **pvalue_results_simple_model.txt** | table of the 2 x 2 t tests with Welch correction from the simple model|
| **pvalue_results_simple_model.RData** | equivalent to the pvalue_results_simple_model.txt but as R object|

| Optional Files (test.log.cor parameter set to TRUE) | Description |
| --- | --- |
| **modified_dataset_for_log2_test.txt** | initial dataset with three columns added to test the effect of the abs(min(yi)) + 1 correction used during log2 transformation|

<br /><br />
## VERSIONS

The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/anova_contrasts/-/tags)

<br /><br />
## LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.

<br /><br />
## CITATION

Not yet published

<br /><br />
## CREDITS

[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Hub-CBD, Institut Pasteur, USR 3756 IP CNRS, Paris, France

<br /><br />
## ACKNOWLEDGEMENTS

Thomas Obadia, Hub-CBD, Institut Pasteur, Paris, France

Stevenn Volant, Hub-CBD, Institut Pasteur, Paris, France

Hugo Varet, Hub-CBD, Institut Pasteur, Paris, France

Amaury Vaysse, Hub-CBD, Institut Pasteur, Paris, France

R and R packages developers

Gitlab developers

Neter J, Kutner MH, Nachtsheim CJ, Wasserman W. Applied Linear Statistical Models 4th Edition. 1990. Erwin Editions. ISBN 0-256-11736-5

<br /><br />
## WHAT'S NEW IN

### v12.7

- .config file saved in the result folder


### v12.6

- error fixed for model2 and type 2 and 3


### v12.5

- error fixed so that now quali columns of required.var are factors when FAKE_VAR column is added


### v12.4

- Messages improved in graphics
- Messages improved for aliasing and nesting


### v12.3

- Nesting detection with single value returns warning instead of error


### v12.2

- Warning message added in title of pre analyse of the response var when log2 transfo: NEG VALUES ARE REMOVED
- Nesting detection added


### v12.1

- Bug fixed in ref.class


### v12.0

- New ref.class parameter to set the reference class of the contrast.kind = "contr.treatment" estimate computations
- 

### v11.4

- lme4::lmer replaced by lmerTest::lmer to get the p values of the fixed effects in mixed models


### v11.3

- Bug fixed with p.value.on.single.class


### v11.2

- modified data (log2) graphes improved
- boxplots improved


### v11.1

- Bug fixed with p.value.on.single.class


### v11.0

- new correcting_factor parameter added (for log2 correction)


### v10.0

- anova_contrasts.config file renamed anova_contrasts.rconfig file


### v9.0

- bug fixed in the checking of model1 writting
- limma_v1.0.R added
- anova_contrasts.config file updated


### v8.5

- bug fixed in the checking of model3 writting


### v8.4

- bugs fixed
- mean.quali parameter improved


### v8.3

- model.family parameter upgraded so that it can use Gamma(link = log)


### v8.2

- Log2 qqplot added


### v8.1

- Restriction added for dot.categ, row.facet and col.facet parameters


### v8.0

- n.limit parameter added to avoid the dots in graphs when n is big


### v7.11

- Graphical analysis of the response variable, independently of the predictive variables


### v7.10.0

1) Now t tests are more stringent: not performed when sd = 0 in one class at least


### v7.9.0

1) bugs fixed


### v7.8.0

1) bugs fixed and interaction.class.rm parameter added


### v7.7.0

1) bugs fixed


### v7.6.0

1) all the parameter checkings have been incorporated


### v7.5.0

1) simple model display improved


### v7.4.0

1) dot.categ parameter added to color the dots according to a variable of the model

2) bug in the simple.model output corrected


### v7.3.0

1) a bug corrected in mean tables (NA indicates aliasing, not 0)


### v7.2.0

1) t tests now deal with single value in classes


### v7.1.0

1) the other way to compute the p values for mixed model and large dataset is now correctly displayed


### v7.0.0

1) a new parameter added y.tick.nb to solve an error that can occurs with fun_gg_boxplot()


### v6.1.0

1) a new significant column (with NS or \* added in p values tables)


### v6.0.0

1) a new parameter added: p.value.on.single.class, and contrast comparisons only for this parameter

2) t-test dans simple model strongly improved


### v5.11.1

1) improvement of the report again


### v5.11.0

1) improvement of the report


### v5.10.0

1) emmeans computation clarified


### v5.9.1

1) a sentence changed in the report


### v5.9.0

1) margin means added with a better display

2) emmeans table now with more decimals for means


### v5.8.0

1) margin means added

2) Messages and report improved


### v5.7.0

1) small bug corrected regarding predict.var.class.rm


### v5.6.0

1) fun_gg_boxplot graphs improved (theme removed in the add argument to do not overwrite x.angle)


### v5.5.0

1) updated for cute v10.9.0


### v5.4.0

1) debugged for the analysis of predictive variables taken independently


### v5.3.0

1) n now indicated in the results


### v5.2.0

1) config file now present in the result folder


### v5.1.0

1) Bug corrected


### v5.0.0

1) comparison of difference of effects (in model with interaction) added in emmean contrast analysis


### v4.15.0

1) bug fixed for welch test p value display


### v4.14.0

1) clearer messages added


### v4.13.0

1) bug fixed for welch test p value display


### v4.12.0

1) bug fixed for welch test p value display


### v4.11.0

1) bug fixed concerning the number of predictive variables detected


### v4.10.0

1) negative and 0 values can be removed before log transfo


### v4.9.1

1) t test added for one-way anova (improved)


### v4.9.0

1) t test added for one way anova


### v4.8.0

1) messages of ancova improved


### v4.7.0

1) now possibility to deal with ancova


### v4.6.0

1) Improvement of Type I,II,III messages for all the 3 models (continued)


### v4.5.0

1) Improvement of Type I,II,III messages for all the 3 models


### v4.4.0

1) type III anova added for model1


### v4.3.0

1) response.var.not.log2 repoved from analysis of factors independently


### v4.2.0

1) New response.var.class.rm parameter added


### v4.1.0

1) No log transfo in the simple model


### v4.0.0

1) Simple model added

2) R v4.0.5


### v3.3.0

1) bug removed in contingency tables


### v3.2.0

1) contingency tables improved


### v3.1.0

1) bug removed


### v3.0.0

1) test.log.cor parameter modified

2) part debuged

3) Display of contingency table improved


### v2.0.0

Upgrade for R4.0.2, among other improvements


### v1.0.0

Everything



